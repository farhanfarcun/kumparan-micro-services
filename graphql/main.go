package main

import (
	"gitlab.com/farhanfarcun/kumparan/graphql/config"
	"io/ioutil"
	"net/http"
	"time"

	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	log "github.com/sirupsen/logrus"
	"gitlab.com/farhanfarcun/kumparan/graphql/resolver"
	client "gitlab.com/farhanfarcun/kumparan/graphql/client/grpc"
)

type query struct{}

func (*query) Hello() string {
	return "Hello, world!"
}

func main() {
	config := config.InitConfig()
	s, err := getSchema("./schema/schema.graphql")
	if err != nil {
		panic(err)
	}

	articleService := client.NewGrpcArticleServiceClient(config.GrpcArticleServicePort)
	authorService := client.NewGrpcAuthorServiceClient(config.GrpcAuthorServicePort)
	resolver := &resolver.Resolver{Article: articleService, Author: authorService}
	schema := graphql.MustParseSchema(s, resolver)

	mux := http.NewServeMux()

	mux.Handle("/query", &relay.Handler{Schema: schema})

	log.WithFields(log.Fields{"time": time.Now()}).Info("starting server")
	log.Fatal(http.ListenAndServe(config.GraphQLServerPort, logged(mux)))
}

func getSchema(path string) (string, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

// logging middleware
func logged(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now().UTC()

		next.ServeHTTP(w, r)

		log.WithFields(log.Fields{
			"path":    r.RequestURI,
			"IP":      r.RemoteAddr,
			"elapsed": time.Now().UTC().Sub(start),
		}).Info()
	})
}
