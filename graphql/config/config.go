package config

// Config contains application config
type Config struct {
	GrpcArticleServicePort string
	GrpcAuthorServicePort  string
	GraphQLServerPort      string
}

// InitConfig initialize new application config
func InitConfig() *Config {
	return &Config{
		GrpcArticleServicePort: "article:8081",
		GrpcAuthorServicePort:  "author:8080",
		GraphQLServerPort: ":3000",
	}
}
