package resolver

import (
	"context"
	pb "gitlab.com/farhanfarcun/kumparan/graphql/proto"	
)

// ArticleResolver is to resolve article
type ArticleResolver struct {
	article *pb.ArticleResponse
	author  *pb.AuthorResponse
}

// ID to resolve id
func (r *ArticleResolver) ID(ctx context.Context) *string {
	result := r.article.Id
	return &result
}

// Name is to resolve
func (r *ArticleResolver) Name(ctx context.Context) *string {
	return &r.article.Name
}

// Slug is to resolve slug name
func (r *ArticleResolver) Slug(ctx context.Context) *string {
	return &r.article.Slug
}

// Body is to resolve body
func (r *ArticleResolver) Body(ctx context.Context) *string {
	return &r.article.Body
}

// CreatedAt is to  created
func (r *ArticleResolver) CreatedAt(ctx context.Context) *string {
	result := r.article.CreatedAt
	return &result
}

// UpdatedAt is
func (r *ArticleResolver) UpdatedAt(ctx context.Context) *string {
	result := r.article.UpdatedAt
	return &result
}

// Author is article Author
func (r *ArticleResolver) Author(ctx context.Context) *AuthorResolver {
	return &AuthorResolver{
		author: r.author,
	}
}
