package resolver

import (
	"context"
	pb "gitlab.com/farhanfarcun/kumparan/graphql/proto"
)

// AuthorInput input result
type AuthorInput struct {
	Name  *string
	Email *string
}

// CreateAuthor to create author
func (r *Resolver) CreateAuthor(ctx context.Context, args struct{ Author AuthorInput }) (*AuthorResolver, error) {
	request := &pb.AuthorRequest{
		Name: *args.Author.Name,
		Email: *args.Author.Email,
	}
	result, err := r.Author.CreateAuthor(context.Background(), request)
	if err != nil {
		return nil, err
	}
	resolver := AuthorResolver{author: result}
	return &resolver, nil
}
