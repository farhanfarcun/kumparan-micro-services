package resolver

import (
	"context"
	pb "gitlab.com/farhanfarcun/kumparan/graphql/proto"
)

// AuthorResolver is to resolve Author
type AuthorResolver struct {
	author *pb.AuthorResponse
	articles []*pb.ArticleResponse
}

// ID to resolve id
func (r *AuthorResolver) ID(ctx context.Context) *string {
	result := r.author.Id
	return &result
}

// Name is to resolve name
func (r *AuthorResolver) Name(ctx context.Context) *string {
	return &r.author.Name
}

// Email is to resolve email
func (r *AuthorResolver) Email(ctx context.Context) *string {
	return &r.author.Email
}

// Articles is to resolve articles
func (r *AuthorResolver) Articles(ctx context.Context) *[]*ArticleResolver {
	result := make([]*ArticleResolver, len(r.articles))
	for i := range r.articles {
		author := r.author
		result[i] = &ArticleResolver{
			article: r.articles[i],
			author:  author,
		}
	}
	return &result
}
