package resolver

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"

	pb "gitlab.com/farhanfarcun/kumparan/graphql/proto"
)

// GetAuthors is to get Authors
func (r *Resolver) GetAuthors(ctx context.Context) (*[]*AuthorResolver, error) {
	authors, err := r.Author.GetAllAuthor(context.Background(), &empty.Empty{})
	if err != nil {
		return nil, err
	}

	result := make([]*AuthorResolver, len(authors.Data))
	for i, v := range authors.Data {
		articles, _ := r.Article.GetAllArticleByUserID(context.Background(), &pb.ArticleRequest{Id: v.Id})
		result[i] = &AuthorResolver{
			author: v,
			articles: articles.Data,
		}
	}
	return &result, err
}

// GetAuthorByID is to get Author by id
func (r *Resolver) GetAuthorByID(ctx context.Context, args struct{ ID string }) (*AuthorResolver, error) {
	author, err := r.Author.GetAuthorByID(context.Background(), &pb.AuthorRequest{Id: args.ID})
	articles, _ := r.Article.GetAllArticleByUserID(context.Background(), &pb.ArticleRequest{Id: args.ID})
	if err != nil {
		return nil, err
	}
	return &AuthorResolver{
		author: author,
		articles: articles.Data,
	}, nil
}
