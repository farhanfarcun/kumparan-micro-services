package resolver

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"

	"github.com/pkg/errors"
	pb "gitlab.com/farhanfarcun/kumparan/graphql/proto"
)

// GetArticles is to get articles
func (r *Resolver) GetArticles(ctx context.Context) (*[]*ArticleResolver, error) {
	response, err := r.Article.GetAllArticle(context.Background(), &empty.Empty{})
	if err != nil {
		return nil, err
	}

	authorsID := make([]string, len(response.Data))
	for i, v := range response.Data {
		authorsID[i] = v.UserId
	}
	authors, _ := r.Author.GetAuthorsByID(context.Background(), &pb.AuthorIDList{Data: authorsID})

	result := make([]*ArticleResolver, len(response.Data))
	for i, v := range response.Data {
		result[i] = &ArticleResolver{
			article: response.Data[i],
			author:  authors.Data[v.UserId],
		}
	}
	return &result, err
}

// GetArticleByID is to find article by id
func (r *Resolver) GetArticleByID(ctx context.Context, args struct{ ID string }) (*ArticleResolver, error) {
	article, err := r.Article.GetArticleByID(context.Background(), &pb.ArticleRequest{Id: args.ID})
	if err != nil {
		return nil, errors.Wrap(err, "GetArticleByID")
	}
	author, _ := r.Author.GetAuthorByID(context.Background(), &pb.AuthorRequest{Id: article.UserId})
	result := ArticleResolver{article: article, author: author}
	return &result, nil
}

// GetArticlesByUserID return articles filter by user id
func (r *Resolver) GetArticlesByUserID(ctx context.Context, args struct{ ID string }) (*[]*ArticleResolver, error) {
	response, err :=  r.Article.GetAllArticleByUserID(context.Background(), &pb.ArticleRequest{Id: args.ID})
	if err != nil {
		return nil, err
	}

	author, _ := r.Author.GetAuthorByID(context.Background(), &pb.AuthorRequest{Id: args.ID})
	result := make([]*ArticleResolver, len(response.Data))
	for i := range response.Data {
		result[i] = &ArticleResolver{
			article: response.Data[i],
			author:  author,
		}
	}
	return &result, err
}