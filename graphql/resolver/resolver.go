package resolver

import (
	pb "gitlab.com/farhanfarcun/kumparan/graphql/proto"
)

// Resolver is to resolve
type Resolver struct {
	Author  pb.AuthorServiceClient
	Article pb.ArticleServiceClient
}
