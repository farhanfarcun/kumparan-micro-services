package resolver

import (
	"context"

	pb "gitlab.com/farhanfarcun/kumparan/graphql/proto"
)

// ArticleInput input result
type ArticleInput struct {
	Name   *string
	Body   *string
	UserID *string
}

// CreateArticle to create article with author
func (r *Resolver) CreateArticle(ctx context.Context, args struct{ Article ArticleInput }) (*ArticleResolver, error) {
	author, err := r.Author.GetAuthorByID(context.Background(), &pb.AuthorRequest{Id: *args.Article.UserID})
	if err != nil {
		return nil, err
	}

	request := &pb.ArticleRequest{
		Name:   *args.Article.Name,
		Body:   *args.Article.Body,
		UserId: *args.Article.UserID,
	}

	result, err := r.Article.CreateArticle(context.Background(), request)

	if err != nil {
		return nil, err
	}

	resolver := ArticleResolver{article: result, author: author}

	return &resolver, nil
}
