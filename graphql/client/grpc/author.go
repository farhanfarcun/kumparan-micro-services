package grpc

import (
	"log"
	"google.golang.org/grpc"
	pb "gitlab.com/farhanfarcun/kumparan/graphql/proto"
)

// NewGrpcAuthorServiceClient return new grpc author service client
func NewGrpcAuthorServiceClient(port string) pb.AuthorServiceClient {
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	return pb.NewAuthorServiceClient(conn)
}
