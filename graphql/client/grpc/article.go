package grpc

import (
	"log"
	"google.golang.org/grpc"
	pb "gitlab.com/farhanfarcun/kumparan/graphql/proto"
)

// NewGrpcArticleServiceClient return new grpc article service client
func NewGrpcArticleServiceClient(port string) pb.ArticleServiceClient {
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	return pb.NewArticleServiceClient(conn)
}
