module gitlab.com/farhanfarcun/kumparan/graphql

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/graph-gophers/graphql-go v0.0.0-20190724201507-010347b5f9e6
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
	gitlab.kumparan.com/luthfi.aida/minikum v0.0.0-20190717022722-f6def546aeef
	golang.org/x/sys v0.0.0-20190606165138-5da285871e9c // indirect
	google.golang.org/grpc v1.22.1
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
