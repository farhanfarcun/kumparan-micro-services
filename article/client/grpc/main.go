package main

import (
	"context"
	"log"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/farhanfarcun/kumparan/article/config"
	pb "gitlab.com/farhanfarcun/kumparan/article/proto"
	"google.golang.org/grpc"
)

func initArticleServiceClient(port string) pb.ArticleServiceClient {
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	return pb.NewArticleServiceClient(conn)
}

func main() {
	config := config.InitConfig()
	client := initArticleServiceClient(config.GRPCServerPort)
	res, err := client.CreateArticle(context.Background(), &pb.ArticleRequest{Name: "sss  ss", Body: "  ss  "})
	if err != nil {
		log.Println(err)
	}
	searchId := res.Id
	result, err := client.GetArticleByID(context.Background(), &pb.ArticleID{Id: searchId})
	if err != nil {
		log.Println(err)
	}
	if res.Id == result.Id {
		log.Println("WORKS!!")
	}
	result1, err := client.GetAllArticle(context.Background(), &empty.Empty{})
	if err != nil {
		log.Println(err)
	}
	log.Println()
	log.Println()
	log.Println(result1)
}
