package storage

import "gitlab.com/farhanfarcun/kumparan/article/domain"

// ArticleStorage is article storage in memory
type ArticleStorage struct {
	Articles []domain.Article
}

// NewArticleStorage is to create article storage
func NewArticleStorage() *ArticleStorage {
	return &ArticleStorage{}
}
