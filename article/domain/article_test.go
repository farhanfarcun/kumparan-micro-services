package domain

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestCreateArticle(t *testing.T) {
	t.Run("Can Create Article", func(t *testing.T) {
		//Given
		name := "riski midi"
		body := "bodi riski midi"

		//When
		article, err := CreateArticle(name, body)

		//Then
		assert.NoError(t, err)
		assert.Equal(t, article.Name, name)
	})
	t.Run("Can't Create Article When Name Empty", func(t *testing.T) {
		//Given
		name := ""
		body := "bodi riski midi"

		//When
		_, err := CreateArticle(name, body)

		//Then
		assert.Error(t, err)
	})
}

func TestAttachUserID(t *testing.T) {
	//Given
	article, _ := CreateArticle("name", "body")
	uid := uuid.NewV4()

	//When
	article.AttachUserID(uid)

	//Then
	assert.NotEmpty(t, article.UserID)
}
