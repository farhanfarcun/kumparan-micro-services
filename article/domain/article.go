package domain

import (
	"errors"
	"fmt"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"
	pb "gitlab.com/farhanfarcun/kumparan/article/proto"
)

const (
	// PUBLISHED if the article is published
	PUBLISHED string = "PUBLISHED"
	// DRAFT if the article is draft
	DRAFT string = "DRAFT"
)

// Article is to wrap article
type Article struct {
	ID        uuid.UUID `json:"id"`
	Slug      string    `json:"slug"`
	Name      string    `json:"name"`
	Body      string    `json:"body"`
	Status    string    `json:"status"`
	UserID    uuid.UUID `json:"user_id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

// CreateArticle is to create article instance
func CreateArticle(name, body string) (*Article, error) {
	uid := uuid.NewV4()

	if len(strings.TrimSpace(name)) == 0 {
		return &Article{}, errors.New("Error Name Empty")
	}

	slug := GenerateSlug(name)

	return &Article{
		ID:        uid,
		Name:      name,
		Slug:      slug,
		Body:      body,
		Status:    DRAFT,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}, nil

}

// GenerateSlug is generate slug article based on name
func GenerateSlug(name string) string {
	slug := strings.TrimSpace(name)
	slug = strings.ReplaceAll(slug, " ", "-")

	return strings.ToLower(slug)
}

// AttachUserID for given article
func (article *Article) AttachUserID(id uuid.UUID) error {
	article.UserID = id
	article.UpdatedAt = time.Now()

	return nil
}

func (article *Article) Proto() *pb.ArticleResponse {
	return &pb.ArticleResponse{
		Id:        fmt.Sprint(article.ID),
		Name:      article.Name,
		Slug:      article.Slug,
		Body:      article.Body,
		Status:    article.Status,
		CreatedAt: fmt.Sprint(article.CreatedAt),
		UpdatedAt: fmt.Sprint(article.UpdatedAt),
		UserId:    fmt.Sprint(article.UserID),
	}
}
