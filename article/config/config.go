package config

// Config contains application config
type Config struct {
	GRPCServerPort string
}

// InitConfig initialize new application config
func InitConfig() *Config {
	return &Config{
		GRPCServerPort: ":8081",
	}
}
