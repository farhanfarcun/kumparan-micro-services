package service

import (
	"errors"
	"strings"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/farhanfarcun/kumparan/article/domain"
	"gitlab.com/farhanfarcun/kumparan/article/repository"
	"gitlab.com/farhanfarcun/kumparan/article/repository/inmemory"
	"gitlab.com/farhanfarcun/kumparan/article/storage"
)

// ArticleService is
type ArticleService struct {
	Query      repository.ArticleQuery
	Repository repository.ArticleRepository
}

// NewArticleService is to create article services
func NewArticleService() *ArticleService {
	var repo repository.ArticleRepository
	var query repository.ArticleQuery

	db := storage.NewArticleStorage()
	query = inmemory.NewArticleQueryInMemory(db)
	repo = inmemory.NewArticleRepositoryInMemory(db)

	return &ArticleService{
		Query:      query,
		Repository: repo,
	}
}

// GetAllArticles is to find all articles
func (service *ArticleService) GetAllArticles() ([]domain.Article, error) {
	// Process
	result := <-service.Query.GetArticles()
	if result.Error != nil {
		return []domain.Article{}, result.Error
	}

	// Return
	return result.Result.([]domain.Article), nil
}

// GetArticlesByUserID is to find all articles by user id
func (service *ArticleService) GetArticlesByUserID(id string) ([]domain.Article, error) {
	// Process
	result := <-service.Query.GetArticlesByUserID(id)
	if result.Error != nil {
		return []domain.Article{}, result.Error
	}

	// Return
	return result.Result.([]domain.Article), nil
}

// FindArticleByID is to find article by id
func (service *ArticleService) GetArticleByID(uid string) (domain.Article, error) {
	id, err := uuid.FromString(uid)
	if err != nil {
		return domain.Article{}, err
	}

	result := <-service.Query.GetArticleByID(id)
	if result.Error != nil {
		// log.WithFields(log.Fields{"time": time.Now()}).Info("Ini error dari services")
		return domain.Article{}, result.Error
	}

	// log.WithFields(log.Fields{"time": time.Now()}).Info("No error")

	return result.Result.(domain.Article), nil
}

// CreateArticle to create article
func (service *ArticleService) CreateArticle(name, body string) (domain.Article, error) {
	// Process
	name = strings.TrimSpace(name)
	body = strings.TrimSpace(body)

	if len(name) == 0 {
		return domain.Article{}, errors.New("Name cannot be empty")
	}

	if len(body) == 0 {
		return domain.Article{}, errors.New("Body cannot be empty")
	}

	result, err := domain.CreateArticle(name, body)
	if err != nil {
		return domain.Article{}, err
	}

	// Persist
	err = <-service.Repository.Save(result)
	if err != nil {
		return domain.Article{}, err
	}

	return *result, nil
}

// AttachUser to article
func (service *ArticleService) AttachUser(articleID, userID string) error {
	// Process
	article, err := service.GetArticleByID(articleID)
	if err != nil {
		return err
	}
	id, _ := uuid.FromString(userID)
	article.AttachUserID(id)
	// Persist

	err = <-service.Repository.Save(&article)
	if err != nil {
		return err
	}
	return nil
}
