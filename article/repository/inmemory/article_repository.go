package inmemory

import (
	"gitlab.com/farhanfarcun/kumparan/article/domain"
	"gitlab.com/farhanfarcun/kumparan/article/repository"
	"gitlab.com/farhanfarcun/kumparan/article/storage"
)

// ArticleRepositoryInMemory is article repository implementation in memory
type ArticleRepositoryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleRepositoryInMemory is to create ArticleRepositoryInMemory instance
func NewArticleRepositoryInMemory(storage *storage.ArticleStorage) repository.ArticleRepository {
	return &ArticleRepositoryInMemory{Storage: storage}
}

// Save is for save article
func (repo *ArticleRepositoryInMemory) Save(article *domain.Article) <-chan error {
	result := make(chan error)

	go func() {
		found := false
		for i, v := range repo.Storage.Articles {
			if v.ID == article.ID {
				repo.Storage.Articles[i] = *article
				found = true
				break
			}
		}

		if !found {
			repo.Storage.Articles = append(repo.Storage.Articles, *article)
		}
		result <- nil
		close(result)
	}()

	return result
}
