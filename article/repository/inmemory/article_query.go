package inmemory

import (
	"errors"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/farhanfarcun/kumparan/article/domain"
	"gitlab.com/farhanfarcun/kumparan/article/repository"
	"gitlab.com/farhanfarcun/kumparan/article/storage"
)

// ArticleQueryInMemory is article query implementation in memory
type ArticleQueryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleQueryInMemory is to Create Instance ArticleQueryInMemory
func NewArticleQueryInMemory(storage *storage.ArticleStorage) repository.ArticleQuery {
	return &ArticleQueryInMemory{Storage: storage}
}

// GetArticles is to find published article by slug
func (query *ArticleQueryInMemory) GetArticles() <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		result <- repository.QueryResult{
			Result: query.Storage.Articles,
		}
		close(result)
	}()

	return result
}

// GetArticlesByUserID is to find article by user id
func (query *ArticleQueryInMemory) GetArticlesByUserID(id string) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		tempStorage := []domain.Article{}
		for _, v := range query.Storage.Articles {
			if fmt.Sprint(v.UserID) == id {
				tempStorage = append(tempStorage, v)
			}
		}
		result <- repository.QueryResult{
			Result: tempStorage,
		}
		close(result)
	}()

	return result
}

// GetArticleByID is to find article by id
func (query *ArticleQueryInMemory) GetArticleByID(id uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		article := domain.Article{}
		for _, item := range query.Storage.Articles {
			if id == item.ID {
				article = item
			}
		}

		if article.Name == "" {
			result <- repository.QueryResult{Error: errors.New("Article not found")}
		} else {
			result <- repository.QueryResult{Result: article}
		}

		close(result)
	}()

	return result
}
