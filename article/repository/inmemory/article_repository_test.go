package inmemory

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/farhanfarcun/kumparan/article/domain"
	"gitlab.com/farhanfarcun/kumparan/article/storage"
)

func TestCanSaveArticleInRepository(t *testing.T) {
	// Given
	articleUID := uuid.NewV4()
	article := domain.Article{
		ID:     articleUID,
		Name:   "Foobar",
		Slug:   "foobar",
		Body:   "body article",
		Status: domain.PUBLISHED,
	}

	t.Run("Save article", func(t *testing.T) {
		db := storage.NewArticleStorage()
		repo := NewArticleRepositoryInMemory(db)
		err := <-repo.Save(&article)
		assert.Nil(t, err)
		assert.Equal(t, 4, len(db.Articles))
	})

	t.Run("Update article", func(t *testing.T) {
		db := storage.NewArticleStorage()
		db.Articles = append(db.Articles, article)
		repo := NewArticleRepositoryInMemory(db)
		err := <-repo.Save(&article)
		assert.Nil(t, err)
		assert.Equal(t, 4, len(db.Articles))
	})
}
