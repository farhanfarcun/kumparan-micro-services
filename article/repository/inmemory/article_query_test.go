package inmemory

import (
	"fmt"
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/farhanfarcun/kumparan/article/domain"
	"gitlab.com/farhanfarcun/kumparan/article/repository"
	"gitlab.com/farhanfarcun/kumparan/article/storage"
)

func TestCanGetAllArticle(t *testing.T) {
	// Given
	done := make(chan bool)

	article1UID := uuid.NewV4()
	article1 := domain.Article{
		ID:     article1UID,
		Name:   "Foobar",
		Slug:   "foobar",
		Body:   "body article",
		Status: domain.PUBLISHED,
	}

	article2UID := uuid.NewV4()
	article2 := domain.Article{
		ID:     article2UID,
		Name:   "Foobar 2",
		Slug:   "foobar-2",
		Body:   "body article 2",
		Status: domain.PUBLISHED,
	}

	storage := storage.ArticleStorage{
		Articles: []domain.Article{
			article1, article2,
		},
	}

	query := NewArticleQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.GetArticles()
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, storage.Articles, result.Result.([]domain.Article))
}

func TestGetAllArticleByUserID(t *testing.T) {
	// Given
	done := make(chan bool)

	article1UID := uuid.NewV4()
	user1UUID := uuid.NewV4()
	article1 := domain.Article{
		ID:     article1UID,
		Name:   "Foobar",
		Slug:   "foobar",
		Body:   "body article",
		UserID: user1UUID,
		Status: domain.PUBLISHED,
	}

	article2UID := uuid.NewV4()
	user2UUID := uuid.NewV4()
	article2 := domain.Article{
		ID:     article2UID,
		Name:   "Foobar 2",
		Slug:   "foobar-2",
		Body:   "body article 2",
		UserID: user2UUID,
		Status: domain.PUBLISHED,
	}

	storage := storage.ArticleStorage{
		Articles: []domain.Article{
			article1, article2,
		},
	}

	query := NewArticleQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.GetArticlesByUserID(fmt.Sprint(user1UUID))
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, storage.Articles[0], result.Result.([]domain.Article)[0])
}

func TestFindArticleByID(t *testing.T) {
	t.Run("Can Find Article By Id", func(t *testing.T) {
		//Given
		done := make(chan bool)

		article1UID := uuid.NewV4()
		article1 := domain.Article{
			ID:     article1UID,
			Name:   "Foobar",
			Slug:   "foobar",
			Body:   "body article",
			Status: domain.PUBLISHED,
		}

		article2UID := uuid.NewV4()
		article2 := domain.Article{
			ID:     article2UID,
			Name:   "Foobar 2",
			Slug:   "foobar-2",
			Body:   "body article 2",
			Status: domain.PUBLISHED,
		}

		storage := storage.ArticleStorage{
			Articles: []domain.Article{
				article1, article2,
			},
		}

		query := NewArticleQueryInMemory(&storage)
		result := repository.QueryResult{}

		// When
		go func() {
			result = <-query.GetArticleByID(article2UID)
			done <- true
		}()

		<-done

		//Then
		assert.Equal(t, storage.Articles[1], result.Result.(domain.Article))
	})

	t.Run("Can't Find Article By Id", func(t *testing.T) {
		//Given
		done := make(chan bool)
		article3UID := uuid.NewV4()

		article1UID := uuid.NewV4()
		article1 := domain.Article{
			ID:     article1UID,
			Name:   "Foobar",
			Slug:   "foobar",
			Body:   "body article",
			Status: domain.PUBLISHED,
		}

		article2UID := uuid.NewV4()
		article2 := domain.Article{
			ID:     article2UID,
			Name:   "Foobar 2",
			Slug:   "foobar-2",
			Body:   "body article 2",
			Status: domain.PUBLISHED,
		}

		storage := storage.ArticleStorage{
			Articles: []domain.Article{
				article1, article2,
			},
		}

		query := NewArticleQueryInMemory(&storage)
		result := repository.QueryResult{}

		// When
		go func() {
			result = <-query.GetArticleByID(article3UID)
			done <- true
		}()

		<-done

		//Then
		assert.Equal(t, "Article not found", result.Error.Error())
	})
}
