package repository

import uuid "github.com/satori/go.uuid"

// QueryResult is to wrap query result
type QueryResult struct {
	Result interface{}
	Error  error
}

// ArticleQuery is contract for article query
type ArticleQuery interface {
	// single
	GetArticles() <-chan QueryResult
	GetArticlesByUserID(string) <-chan QueryResult
	GetArticleByID(uuid.UUID) <-chan QueryResult
}
