package repository

import "gitlab.com/farhanfarcun/kumparan/article/domain"

// ArticleRepository is wrap contract article repository
type ArticleRepository interface {
	Save(article *domain.Article) <-chan error
}
