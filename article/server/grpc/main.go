package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/farhanfarcun/kumparan/article/config"
	"google.golang.org/grpc"

	pb "gitlab.com/farhanfarcun/kumparan/article/proto"
	"gitlab.com/farhanfarcun/kumparan/article/service"
)

// ArticleServer is GRPC ArticleServiceServer implementation
type ArticleServer struct {
	Service service.ArticleService
}

// NewArticleServer create new ArticleServer
func NewArticleServer(service service.ArticleService) pb.ArticleServiceServer {
	return &ArticleServer{
		Service: service,
	}
}

// CreateArticle create new Article
func (s *ArticleServer) CreateArticle(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	article, err := s.Service.CreateArticle(param.Name, param.Body)
	err = s.Service.AttachUser(fmt.Sprint(article.ID), param.UserId)
	if err != nil {
		return nil, err
	}
	return article.Proto(), nil
}

// GetArticleByID return article by id
func (s *ArticleServer) GetArticleByID(ctx context.Context, param *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	article, err := s.Service.GetArticleByID(param.Id)
	if err != nil {
		return nil, err
	}
	return article.Proto(), nil
}

// GetAllArticle return all article
func (s *ArticleServer) GetAllArticle(ctx context.Context, param *empty.Empty) (*pb.Articles, error) {
	articles, err := s.Service.GetAllArticles()
	if err != nil {
		return nil, err
	}

	var result pb.Articles
	for i := range articles {
		result.Data = append(result.Data, articles[i].Proto())
	}

	return &result, nil
}

// GetAllArticleByUserID return all article by user id
func (s *ArticleServer) GetAllArticleByUserID(ctx context.Context, param *pb.ArticleRequest) (*pb.Articles, error) {
	articles, err := s.Service.GetArticlesByUserID(param.Id)
	if err != nil {
		return nil, err
	}

	var result pb.Articles
	for i := range articles {
		result.Data = append(result.Data, articles[i].Proto())
	}

	return &result, nil
}

func main() {
	config := config.InitConfig()
	grpc := grpc.NewServer()
	server := NewArticleServer(*service.NewArticleService())
	pb.RegisterArticleServiceServer(grpc, server)

	log.Println("Starting grpc server at", config.GRPCServerPort)
	listen, err := net.Listen("tcp", config.GRPCServerPort)
	if err != nil {
		log.Fatalf("Could not listen to %s: %v", config.GRPCServerPort, err)
	}
	log.Fatal(grpc.Serve(listen))
}
