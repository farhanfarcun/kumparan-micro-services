package service

import (
	"errors"
	"strings"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/farhanfarcun/kumparan/author/domain"
	"gitlab.com/farhanfarcun/kumparan/author/repository"
	"gitlab.com/farhanfarcun/kumparan/author/repository/inmemory"
	"gitlab.com/farhanfarcun/kumparan/author/storage"
)

// AuthorService is
type AuthorService struct {
	Query      repository.AuthorQuery
	Repository repository.AuthorRepository
}

// NewAuthorService is to create Author services
func NewAuthorService() *AuthorService {
	var repo repository.AuthorRepository
	var query repository.AuthorQuery

	db := storage.NewAuthorStorage()
	query = inmemory.NewAuthorQueryInMemory(db)
	repo = inmemory.NewAuthorRepositoryInMemory(db)

	return &AuthorService{
		Query:      query,
		Repository: repo,
	}
}

// GetAllAuthors is to find all Authors
func (service *AuthorService) GetAllAuthors() ([]domain.Author, error) {
	// Process
	result := <-service.Query.GetAuthors()
	if result.Error != nil {
		return []domain.Author{}, result.Error
	}

	// Return
	return result.Result.([]domain.Author), nil
}

// GetAuthorByID is to Get Author by id
func (service *AuthorService) GetAuthorByID(uid string) (domain.Author, error) {
	id, err := uuid.FromString(uid)
	if err != nil {
		return domain.Author{}, err
	}

	result := <-service.Query.GetAuthorByID(id)
	if result.Error != nil {
		// log.WithFields(log.Fields{"time": time.Now()}).Info("Ini error dari services")
		return domain.Author{}, result.Error
	}

	// log.WithFields(log.Fields{"time": time.Now()}).Info("No error")

	return result.Result.(domain.Author), nil
}

// CreateAuthor to create Author
func (service *AuthorService) CreateAuthor(name, email string) (domain.Author, error) {
	// Process
	name = strings.TrimSpace(name)
	email = strings.TrimSpace(email)

	if len(name) == 0 {
		return domain.Author{}, errors.New("Name cannot be empty")
	}

	if len(email) == 0 {
		return domain.Author{}, errors.New("Email cannot be empty")
	}

	result, err := domain.CreateAuthor(name, email)
	if err != nil {
		return domain.Author{}, err
	}

	// Persist
	err = <-service.Repository.Save(result)
	if err != nil {
		return domain.Author{}, err
	}

	return *result, nil
}
