package domain

import (
	"fmt"
	uuid "github.com/satori/go.uuid"
	pb "gitlab.com/farhanfarcun/kumparan/author/proto"
)

// Author wrapper
type Author struct {
	ID    uuid.UUID
	Name  string
	Email string
}

// Proto return author in protobuf format
func (a *Author) Proto() *pb.AuthorResponse {
	return &pb.AuthorResponse{
		Id: fmt.Sprint(a.ID),
		Name: a.Name,
		Email: a.Email,
	}
}

// CreateAuthor is to create author
func CreateAuthor(name, email string) (*Author, error) {
	uid := uuid.NewV4()

	return &Author{
		ID:    uid,
		Name:  name,
		Email: email,
	}, nil
}
