package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateAuthor(t *testing.T) {
	t.Run("Can Create Author", func(t *testing.T) {
		//Given
		name := "riski midi"
		email := "riskimidi@gmail.com"

		//When
		author, err := CreateAuthor(name, email)

		//Then
		assert.NoError(t, err)
		assert.Equal(t, author.Name, name)
	})
}
