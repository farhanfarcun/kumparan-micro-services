package main

import (
	"context"
	"log"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/farhanfarcun/kumparan/author/config"
	pb "gitlab.com/farhanfarcun/kumparan/author/proto"
	"google.golang.org/grpc"
)

func initAuthorServiceClient(port string) pb.AuthorServiceClient {
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	return pb.NewAuthorServiceClient(conn)
}

func main() {
	config := config.InitConfig()
	client := initAuthorServiceClient(config.GRPCServerPort)
	res, err := client.CreateAuthor(context.Background(), &pb.AuthorRequest{Name: " dsdsad ", Email: " sdsd "})
	if err != nil {
		log.Println(err)
	}
	log.Println(res)

	res, err = client.GetAuthorByID(context.Background(), &pb.AuthorID{Id: res.Id})
	if err != nil {
		log.Println(err)
	}
	log.Println(res)

	res1, err := client.GetAllAuthor(context.Background(), &empty.Empty{})
	if err != nil {
		log.Println(err)
	}
	log.Println(res1)
}
