package main

import (
	"context"
	"log"
	"net"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/farhanfarcun/kumparan/author/config"
	"google.golang.org/grpc"

	pb "gitlab.com/farhanfarcun/kumparan/author/proto"
	"gitlab.com/farhanfarcun/kumparan/author/service"
)

// AuthorServer is GRPC AuthorServiceServer implementation
type AuthorServer struct {
	Service service.AuthorService
}

// NewAuthorServer create new AuthorServer
func NewAuthorServer(service service.AuthorService) pb.AuthorServiceServer {
	return &AuthorServer{
		Service: service,
	}
}

// CreateAuthor create new author
func (s *AuthorServer) CreateAuthor(ctx context.Context, param *pb.AuthorRequest) (*pb.AuthorResponse, error) {
	author, err := s.Service.CreateAuthor(param.Name, param.Email)
	if err != nil {
		return nil, err
	}
	return author.Proto(), nil
}

// GetAuthorByID return author by id
func (s *AuthorServer) GetAuthorByID(ctx context.Context, param *pb.AuthorRequest) (*pb.AuthorResponse, error) {
	author, err := s.Service.GetAuthorByID(param.Id)
	if err != nil {
		return nil, err
	}
	return author.Proto(), nil
}

// GetAllAuthor return all author
func (s *AuthorServer) GetAllAuthor(ctx context.Context, param *empty.Empty) (*pb.Authors, error) {
	authors, err := s.Service.GetAllAuthors()

	if err != nil {
		return nil, err
	}

	var result pb.Authors
	for i := range authors {
		result.Data = append(result.Data, authors[i].Proto())
	}

	return &result, nil
}

// GetAuthorsByID return map authors by id
func (s *AuthorServer) GetAuthorsByID(ctx context.Context, param *pb.AuthorIDList) (*pb.AuthorMap, error) {
	result := make(map[string]*pb.AuthorResponse)

	for _, v := range param.Data {
		a, _ := s.Service.GetAuthorByID(v)
		result[v] = a.Proto()
	}
	return &pb.AuthorMap{Data: result}, nil
}

func main() {
	config := config.InitConfig()
	grpc := grpc.NewServer()
	server := NewAuthorServer(*service.NewAuthorService())
	pb.RegisterAuthorServiceServer(grpc, server)

	log.Println("Starting grpc server at", config.GRPCServerPort)
	listen, err := net.Listen("tcp", config.GRPCServerPort)
	if err != nil {
		log.Fatalf("Could not listen to %s: %v", config.GRPCServerPort, err)
	}
	log.Fatal(grpc.Serve(listen))
}
