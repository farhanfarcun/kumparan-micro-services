package storage

import "gitlab.com/farhanfarcun/kumparan/author/domain"

// AuthorStorage is to create author
type AuthorStorage struct {
	Authors []domain.Author
}

// NewAuthorStorage s
func NewAuthorStorage() *AuthorStorage {
	author1, _ := domain.CreateAuthor("Riski Midi", "riskimidi@icloud.com")
	author2, _ := domain.CreateAuthor("Farhan Ramadhan", "farhanramadhan@icloud.com")

	return &AuthorStorage{
		Authors: []domain.Author{
			*author1,
			*author2,
		},
	}
}
