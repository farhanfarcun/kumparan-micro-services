module gitlab.com/farhanfarcun/kumparan/author

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.3.0
	google.golang.org/grpc v1.22.1
)
