package repository

import uuid "github.com/satori/go.uuid"

// QueryResult is to wrap query result
type QueryResult struct {
	Result interface{}
	Error  error
}

// AuthorQuery is contract for author query
type AuthorQuery interface {
	GetAuthors() <-chan QueryResult
	GetAuthorByID(uuid.UUID) <-chan QueryResult
}
