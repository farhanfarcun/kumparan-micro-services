package repository

import "gitlab.com/farhanfarcun/kumparan/author/domain"

// AuthorRepository is wrap contract Author repository
type AuthorRepository interface {
	Save(author *domain.Author) <-chan error
}
