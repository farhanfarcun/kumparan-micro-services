package inmemory

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/farhanfarcun/kumparan/author/domain"
	"gitlab.com/farhanfarcun/kumparan/author/storage"
)

func TestCanSaveAuthorInRepository(t *testing.T) {
	// Given
	AuthorUID := uuid.NewV4()
	Author := domain.Author{
		ID:    AuthorUID,
		Name:  "Foobar",
		Email: "foobar@gmail.com",
	}

	t.Run("Save Author", func(t *testing.T) {
		db := storage.NewAuthorStorage()
		repo := NewAuthorRepositoryInMemory(db)
		err := <-repo.Save(&Author)
		assert.Nil(t, err)
		assert.Equal(t, 3, len(db.Authors))
	})

	t.Run("Update Author", func(t *testing.T) {
		db := storage.NewAuthorStorage()
		db.Authors = append(db.Authors, Author)
		repo := NewAuthorRepositoryInMemory(db)
		err := <-repo.Save(&Author)
		assert.Nil(t, err)
		assert.Equal(t, 3, len(db.Authors))
	})
}
