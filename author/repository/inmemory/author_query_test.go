package inmemory

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/farhanfarcun/kumparan/author/domain"
	"gitlab.com/farhanfarcun/kumparan/author/repository"
	"gitlab.com/farhanfarcun/kumparan/author/storage"
)

func TestCanGetAllAuthor(t *testing.T) {
	// Given
	done := make(chan bool)

	Author1UID := uuid.NewV4()
	Author1 := domain.Author{
		ID:    Author1UID,
		Name:  "Foobar",
		Email: "foobar@gmail.com",
	}

	Author2UID := uuid.NewV4()
	Author2 := domain.Author{
		ID:    Author2UID,
		Name:  "Foobar 2",
		Email: "foobar2@gmail.com",
	}

	storage := storage.AuthorStorage{
		Authors: []domain.Author{
			Author1, Author2,
		},
	}

	query := NewAuthorQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.GetAuthors()
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, storage.Authors, result.Result.([]domain.Author))
}

func TestFindAuthorByID(t *testing.T) {
	Author1UID := uuid.NewV4()
	Author1 := domain.Author{
		ID:    Author1UID,
		Name:  "Foobar",
		Email: "foobar@gmail.com",
	}

	Author2UID := uuid.NewV4()
	Author2 := domain.Author{
		ID:    Author2UID,
		Name:  "Foobar 2",
		Email: "foobar2@gmail.com",
	}

	t.Run("Can Find Author By Id", func(t *testing.T) {
		//Given
		done := make(chan bool)

		storage := storage.AuthorStorage{
			Authors: []domain.Author{
				Author1, Author2,
			},
		}

		query := NewAuthorQueryInMemory(&storage)
		result := repository.QueryResult{}

		// When
		go func() {
			result = <-query.GetAuthorByID(Author2UID)
			done <- true
		}()

		<-done

		//Then
		assert.Equal(t, storage.Authors[1], result.Result.(domain.Author))
	})

	t.Run("Can't Find Author By Id", func(t *testing.T) {
		//Given
		done := make(chan bool)
		Author3UID := uuid.NewV4()

		storage := storage.AuthorStorage{
			Authors: []domain.Author{
				Author1, Author2,
			},
		}

		query := NewAuthorQueryInMemory(&storage)
		result := repository.QueryResult{}

		// When
		go func() {
			result = <-query.GetAuthorByID(Author3UID)
			done <- true
		}()

		<-done

		//Then
		assert.Equal(t, "Author not found", result.Error.Error())
	})
}
