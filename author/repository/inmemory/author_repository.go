package inmemory

import (
	"gitlab.com/farhanfarcun/kumparan/author/domain"
	"gitlab.com/farhanfarcun/kumparan/author/repository"
	"gitlab.com/farhanfarcun/kumparan/author/storage"
)

// AuthorRepositoryInMemory is Author repository implementation in memory
type AuthorRepositoryInMemory struct {
	Storage *storage.AuthorStorage
}

// NewAuthorRepositoryInMemory is to create AuthorRepositoryInMemory instance
func NewAuthorRepositoryInMemory(storage *storage.AuthorStorage) repository.AuthorRepository {
	return &AuthorRepositoryInMemory{
		Storage: storage,
	}
}

// Save is for save Author
func (repo *AuthorRepositoryInMemory) Save(Author *domain.Author) <-chan error {
	result := make(chan error)

	go func() {
		found := false
		for i, v := range repo.Storage.Authors {
			if v.ID == Author.ID {
				repo.Storage.Authors[i] = *Author
				found = true
				break
			}
		}

		if !found {
			repo.Storage.Authors = append(repo.Storage.Authors, *Author)
		}
		result <- nil
		close(result)
	}()

	return result
}
