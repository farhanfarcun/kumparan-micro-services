package inmemory

import (
	"errors"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/farhanfarcun/kumparan/author/domain"
	"gitlab.com/farhanfarcun/kumparan/author/repository"
	"gitlab.com/farhanfarcun/kumparan/author/storage"
)

// AuthorQueryInMemory is author query implementation in memory
type AuthorQueryInMemory struct {
	Storage *storage.AuthorStorage
}

// NewAuthorQueryInMemory is to Create Instance AuthorQueryInMemory
func NewAuthorQueryInMemory(storage *storage.AuthorStorage) repository.AuthorQuery {
	return &AuthorQueryInMemory{
		Storage: storage,
	}
}

// GetAuthors is to find published Author
func (query *AuthorQueryInMemory) GetAuthors() <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		result <- repository.QueryResult{
			Result: query.Storage.Authors,
		}
		close(result)
	}()

	return result
}

// GetAuthorByID is to find Author by id
func (query *AuthorQueryInMemory) GetAuthorByID(id uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		Author := domain.Author{}
		for _, item := range query.Storage.Authors {
			if id == item.ID {
				Author = item
			}
		}

		if Author.Name == "" {
			result <- repository.QueryResult{Error: errors.New("Author not found")}
		} else {
			result <- repository.QueryResult{Result: Author}
		}

		close(result)
	}()

	return result
}
